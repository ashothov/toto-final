<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    protected $fillable = ['question', 'score'];

    protected $dates = ['deleted_at'];

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}
