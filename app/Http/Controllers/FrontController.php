<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Question;
use App\Result;
use App\User;
use App\Questionnaire;
use Auth;

use Illuminate\Support\Facades\View;

class FrontController extends Controller
{
    public function index()
    {
//        dd(Auth::user()->id);
//        if((Auth::user())){

//        $game_res = Result::where('user_id',Auth::user()->id)->limit(1)->get();
//        $res = $game_res[0]->result;

//        dd($res);
//        }else {
//            $res = '999';
//        }
//        View::share('res', $res);
//        view()->share('res', $res);
//        if(isset(Auth::user()->id)){
//            $game_res = Result::where('user_id',Auth::user()->id)->limit(1)->get();
//            $res = $game_res[0]->result;
//        }else {
//            $res = '';
//        }
//        $ogmeta ='Խաղի արդյունքում ես հավաքել եմ '. $res.' միավոր';
////        dd($ogmeta);
////                dd($res);

        if (view()->exists('front.index')) {
            return view('front.index');
        }
    }


    public function start_page()
    {
        if (Auth::check()) {
            // ID текущего пользователя
            $current_user_id = Auth::user()->id;
            // Получаем время игры пользователя
            $last_game_time = Result::select('game_time')->where('user_id', $current_user_id)->get();
        }

        $result = null;
        if (!empty($last_game_time[0])) {
            $save_time = $last_game_time[0]->game_time + 86400;

        }
        if (isset($save_time) >= time()) {
            $result = false;
        } else {
            $result = true;
        }

        if (view()->exists('start-page')) {
            return view('start-page', compact('result'));
        }
    }


    public function show_leaders()
    {
        $results = Result::orderBy('result', 'DESC')->get();
        //dd($results);
        foreach ($results as $key => $result) {
            $user = User::select('name')->where('id', $result->user_id)->get();
            $user_info[$key]['name'] = $user[0]->name;
            $user_info[$key]['result'] = $result->result;
            $user_info[$key]['id'] = $result->user_id;
        }
        if (view()->exists('leaders-page')) {
            return view('leaders-page', compact('user_info'));
        }
    }


    public function regulations()
    {
        if (view()->exists('regulations-page')) {
            return view('regulations-page');
        }
    }


    public function start()
    {
        // Генерируем случайные целые числа
        $questionnaires_id = Questionnaire::where('id' ,'>' ,0)->pluck('id')->toArray();
        $random_id = rand(min($questionnaires_id), max($questionnaires_id));

        // Получаем случайный билет
        $questionnaire = Questionnaire::find($random_id);
        $questions_id = json_decode($questionnaire->questions_id);

        // Получаем первый вопрос
        $question = Question::find($questions_id[0]);

        // Получаем ответы на первый вопрос
        $answers = $question->answers;

        if (view()->exists('game-start')) {
            return view('game-start', [
                'question' => $question,
                'answers' => $answers,
                'questionnaire' => $questionnaire,
            ]);
        }
    }


    public function ajax_call(Request $request)
    {
        // Выбираем билет, который уже сдается
        $questionnaire = Questionnaire::find($request->questionnaire_id);
        // Выбираем ID билета, который уже сдается
        $questionnaire_array['questionnaire_id'] = $questionnaire->id;
        // Получаем идентификаторы вопросов из билета, который уже сдается
        $quest_id = json_decode($questionnaire->questions_id);
        // Удаляем идентификатор первого вопроса из массива на который уже ответили

        $key = array_search($request->first_question_id, $quest_id);
        $key++;
        // Выбираем следующий вопрос
        if(array_key_exists($key, $quest_id)) {
            $question = Question::find($quest_id[$key]);
            $questionnaire_array['question_id'] = $question->id;
            $questionnaire_array['question'] = $question->question;
            $questionnaire_array['score'] = $question->score;
            $questionnaire_array['question_number'] = ++$key;

            // Ответы следующего вопроса
            $questionnaire_array['answers'] = $question->answers;

            return $questionnaire_array;
        } else {
            return null;
        }
    }


    public function save_result(Request $request)
    {
        $inputs = $request->except('_token');
        $inputs['game_time'] = time();
//dd($inputs['result']);
        $existing_user = Result::where('user_id', $inputs['user_id'])->first();
//dd($existing_user->result);
        if ($existing_user) {
            if ($inputs['result'] > $existing_user->result) {
                $existing_user->fill($inputs);
                if ($existing_user->update()) {
//                    $game_result = Result::orderBy('id', 'DESC')->limit(1)->get();
                    $game_result = Result::where('user_id',Auth::user()->id)->limit(1)->get();
                    $user_id = $game_result[0]->user_id;
                    $user_info = User::find($user_id);
                    if (view()->exists('score-page')) {
                        return view('score-page', [
                            'game_result' => $game_result,
                            'user_info' => $user_info,
                        ]);
                    }
                }
            } else {
                $existing_user->game_time = time();
                if ($existing_user->update()) {
//                    $game_result = Result::orderBy('id', 'DESC')->limit(1)->get();
//                    $game_result = Result::where('user_id',Auth::user()->id)->limit(1)->get();
//                    dd($game_result);
//                    $user_id = $game_result->user_id;
                    $user_id = Auth::user()->id;
                    $user_info = User::find($user_id);
//                    dd($user_info);
                    $game_result_low = $inputs['result'];
                    if (view()->exists('score-page')) {
                        return view('score-page', [
                            'game_result_low' => $game_result_low,
                            'user_info' => $user_info,
                        ]);
                    }
//                    return redirect('/start-page')->with('status', 'Այս շաբաթ Դուք ավելի շատ միավորներ եք հավաքել քան այս խաղի ժամանակ։ Կրկին փորձեք 24 ժամից։');
                }
            }
        } else {
            $new_user_result = new Result();
            $new_user_result->fill($inputs);
            if ($new_user_result->save()) {
                $game_result = Result::orderBy('id', 'DESC')->limit(1)->get();
                $user_id = $game_result[0]->user_id;
                $user_info = User::find($user_id);
                if (view()->exists('score-page')) {
                    return view('score-page', [
                        'game_result' => $game_result,
                        'user_info' => $user_info,
                    ]);
                }
            }
        }
    }
}
