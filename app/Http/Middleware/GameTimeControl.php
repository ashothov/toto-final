<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Result;

class GameTimeControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // ID текущего пользователя
        $current_user_id = Auth::user()->id;
        // Получаем время игры пользователя
        $last_game_time = Result::select('game_time')->where('user_id', $current_user_id)->first();
        $save_time = 0;
        if (!empty($last_game_time)) {
            $save_time = $last_game_time->game_time + 86400;
        }

        if ($save_time !== 0 && $save_time > time()) {
            return redirect('/start-page')->with('status', 'Դուք կարող եք կրկին խաղալ Ձեր նախորդ խաղից 24 ժամ անց։');
        } else {
            return $next($request);
        }
    }
}
