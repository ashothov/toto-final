<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Result;
use App\Archive;

class ClearingResultTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ClearingResultTable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the results table and enter all the data in the Archive table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $max_result = Result::max('result');
        $min_time = Result::where('result', $max_result)->min('game_time');
        $result = Result::all();
        $data = [];
        foreach ($result as $item) {
            array_push($data, $item->only(['user_id','result','game_time']));
        }
        for ($i = 0; $i < count($data); $i++) {
            // Если в данном массиве присутствует и максимальный результат и минимальное время
            // то присваиваем leader=1 иначе leader = 0
            if ($data[$i]['result'] === $max_result && $data[$i]['game_time'] === $min_time) {
                $data[$i]['leader'] = 1;
            } else {
                $data[$i]['leader'] = 0;
            }
        }
        // Переносим всю информацию игроков прошлой недели в таблицу archives
        foreach ($data as $item) {
            $archive_row = new Archive();
            $archive_row->fill($item);
            $archive_row->save();
        }
        // Очищаем таблицу result
        Result::truncate();
    }
}
