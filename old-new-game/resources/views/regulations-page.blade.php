@extends('layouts.front')

@section('title', 'Regulations page')

@section('content')
    <div class="container">
        <div class="row row-bg">
            <img src="{{ asset('images/ball1.png') }}" alt="Ball">
            <img src="{{ asset('images/ball2.png') }}" alt="Ball">
            <img src="{{ asset('images/ball3.png') }}" alt="Ball">
            <img src="{{ asset('images/tennis.png') }}" alt="Tennis">
            <img src="{{ asset('images/ball4.png') }}" alt="Ball">
            <div class="col-sm-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center">
                <img src="{{ asset('images/image-title.png') }}" alt="Title image">
                <div class="row">
                    <div class="col-sm-12 col-md-10 offset-md-1 text-center">
                        <p class="regulations-p">
                            <span class="regulations-title">Կանոններ</span>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad, at cumque deserunt eum eveniet
                            fuga, incidunt inventore, numquam praesentium quod repellendus sequi suscipit voluptatibus?
                            Ab accusantium eligendi laudantium suscipit voluptatem. Accusantium, adipisci asperiores aut
                            deleniti dicta dignissimos dolorem et facere hic laborum magnam minima nam natus possimus
                            praesentium quaerat quam quo rerum sunt totam. Consectetur cupiditate perferendis quod quos.
                            Adipisci cum deleniti eos et modi mollitia natus nobis non nulla officiis, pariatur porro
                            quae quod rem repellat tenetur voluptatem.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>
@endsection
