@extends('layouts.front')

@section('title', 'Score page')

@section('content')
    <div class="col-sm-12 col-md-6 offset-md-3 text-center">
        <img src="{{ $user_info->avatar }}" alt="">
        <p class="out-score">{{ $user_info->name }}</p>
        <hr>
        <p class="out-score">Ձեր միավորը</p>
        <p class="score-p">{{ $game_result[0]->result }}</p>
        <br>
        <br>
        <br>
        <div onclick="return shareOverrideOGMeta();" class="btn btn-primary clearfix">
            Share Your game result
            <i class="fa fa-facebook"></i>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function shareOverrideOGMeta()
        {
            let result = document.querySelector('p.score-p').innerHTML;
            FB.ui({
                    method: 'share_open_graph',
                    action_type: 'og.shares',
                    action_properties: JSON.stringify({
                        object: {
                            'og:url': 'https://new-game.com/',
                            'og:title': 'Toto Gaming',
                            'og:description': 'В результате Вашей игры Вы набрали '+ result +' баллов',
                            'og:image:width': '400',
                            'og:image:height': '300',
                            'og:image': 'https://www.totogaming.am/img/meta/tg_logo.png'
                        }
                    })
                },
                function (response) {
                    window.location.replace("https://game.it-talents.org/");
                });
        }
    </script>
@endsection
