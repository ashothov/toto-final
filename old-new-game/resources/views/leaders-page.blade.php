@extends('layouts.front')

@section('title', 'Leaders page')

@section('content')
    <div class="container">
        <div class="row row-bg">
            <div class="col-sm-12 col-md-10 offset-md-1 col-lg-6 offset-lg-3 text-center">
                <img src="{{ asset('images/image-title.png') }}" alt="Title image">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                        <div class="liders-block">
                            <h2>ԱՌԱՋԱՏԱՐՆԵՐԸ</h2>
                            @if(isset($user_info) && is_array($user_info))
                            <ol class="list-group liders-list">
                                @foreach($user_info as $item)
                                <li class="list-group-item">
                                    <a href="#">{{ $item['id'] }}. {{ $item['name'] }} <span class="user-score">{{ $item['result'] }}</span></a>
                                </li>
                                @endforeach
                            </ol>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>
@endsection
