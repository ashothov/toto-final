@extends('layouts.app')

@section('title', 'Questionnaire add')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('questionnaire.index') }}">Questionnaires</a>
        </li>
        <li class="breadcrumb-item active">Questionnaire add</li>
    </ol>
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <h5 class="card-header">
                            Questionnaire
                            <span class="text-right text-danger" style="font-size: 14px; margin-bottom: 0;">
                                ( Hold the mouse and drag questions to this block. <i class="fa fa-hand-o-down"></i> )
                            </span>
                        </h5>
                        <div id="sortable1" class="card-body connectedSortable">
                            <form action="{{ route('questionnaire.store') }}" method="post" id="question-form">
                                {{ @csrf_field() }}
                                <input type="hidden" id="question-id-form" name="questions_id[]" value="">
                            </form>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" form="question-form" placeholder="Enter questionnaire title">
                    </div>
                    <button type="submit" form="question-form" class="btn btn-outline-success">Save questionnaire</button>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <h5 class="card-header">Questions</h5>
                        <div id="sortable2" class="card-body connectedSortable">
                            @if (isset($questions) && is_object($questions))
                                @foreach($questions as $question)
                                    @if(!$question->answers->isEmpty())
                                        <div class="alert alert-success" id="{{ $question->id }}" role="alert" style="padding: 5px; font-size: 14px;">
                                            {{ $question->question }}
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
