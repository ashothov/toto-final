@extends('layouts.app')

@section('title', 'Update question')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('question.index') }}">Questions</a>
        </li>
        <li class="breadcrumb-item active">Add question</li>
    </ol>
    <div class="col-sm-12 col-md-10 offset-1">
        <form action="{{ route('question.update', $question->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            {{-- Question --}}
            <div class="form-group">
                <label for="question">Question</label>
                <input type="text" name="question" value="{{ $question->question }}" class="form-control" id="question" placeholder="question text">
            </div>
            {{-- Score --}}
            <div class="form-group">
                <label for="score">Score</label>
                <input type="text" name="score" value="{{ $question->score }}" class="form-control" id="score" placeholder="question score">
            </div>
            <br>
            <hr>
            <br>
            {{-- Answer --}}
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="answer1" value="{{ $answers[0]->answer }}" class="form-control" placeholder="variants of answers...">
                        <input type="hidden" name="answer1_id" value="{{ $answers[0]->id }}">
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label for="answer1_img" class="btn btn-success">
                            Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                        </label>
                        <input type="file" name="images1" class="form-control-file" hidden id="answer1_img">
                        <input type="hidden" name="old_images1" value="{{ $answers[0]->answer_photo }}">
                        <img src="{{ asset('images') . '/' . $answers[0]->answer_photo }}" alt="{{ $answers[0]->answer_photo }}" style="width: 60px;height: 50px;margin-left: 5px;">
                    </div>
                </div>
                <div class="col-sm-12 col-md-1">
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" value="answer1" id="customRadio1" {{ ($answers[0]->truth) ? 'checked' : '' }} name="truth" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio1" style="display: inherit;"></label>
                        </div>
                    </div>
                </div>
            </div>
            <br>

            {{-- Aswer --}}
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="answer2" value="{{ $answers[1]->answer }}" class="form-control" placeholder="variants of answers...">
                        <input type="hidden" name="answer2_id" value="{{ $answers[1]->id }}">

                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label for="answer2_img" class="btn btn-success">
                            Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                        </label>
                        <input type="file" name="images2" class="form-control-file" hidden id="answer2_img">
                        <input type="hidden" name="old_images2" value="{{ $answers[1]->answer_photo }}">
                        <img src="{{ asset('images') . '/' . $answers[1]->answer_photo }}" alt="{{ $answers[1]->answer_photo }}" style="width: 60px;height: 50px;margin-left: 10px;">
                    </div>
                </div>
                <div class="col-sm-12 col-md-1">
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" value="answer2" id="customRadio2" {{ ($answers[1]->truth) ? 'checked' : '' }} name="truth" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio2" style="display: inherit;"></label>
                        </div>
                    </div>
                </div>
            </div>
            <br>

            {{-- Answer --}}
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="answer3" value="{{ $answers[2]->answer }}" class="form-control" placeholder="variants of answers...">
                        <input type="hidden" name="answer3_id" value="{{ $answers[2]->id }}">
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label for="answer3_img" class="btn btn-success">
                            Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                        </label>
                        <input type="file" name="images3" class="form-control-file" hidden id="answer3_img">
                        <input type="hidden" name="old_images3" value="{{ $answers[2]->answer_photo }}">
                        <img src="{{ asset('images') . '/' . $answers[2]->answer_photo }}" alt="{{ $answers[2]->answer_photo }}" style="width: 60px;height: 50px;margin-left: 10px;">
                    </div>
                </div>
                <div class="col-sm-12 col-md-1">
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" value="answer3" id="customRadio3" {{ ($answers[2]->truth) ? 'checked' : '' }} name="truth" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio3" style="display: inherit;"></label>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            {{-- Button --}}
            <button type="submit" class="btn btn-success btn-block">
                Update question
            </button>
        </form>
    </div>
@endsection
