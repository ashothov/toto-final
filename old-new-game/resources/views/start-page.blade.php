@extends('layouts.front')

@section('title', 'Home page')

@section('content')
    <div class="container center-container">
        <div class="row row-bg">
            <img src="{{ asset('images/ball1.png') }}" alt="Ball">
            <img src="{{ asset('images/ball2.png') }}" alt="Ball">
            <img src="{{ asset('images/ball3.png') }}" alt="Ball">
            <img src="{{ asset('images/tennis.png') }}" alt="Tennis">
            <img src="{{ asset('images/ball4.png') }}" alt="Ball">
            <div class="col-sm-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-center">
                <img src="{{ asset('images/image-title.png') }}" alt="Title image">
                <h1>ՍՊՈՐՏ ՏՈՒՐՆԻՐ</h1>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1 text-center">
                        <ul class="list-group home-list">
                            <li class="list-group-item"><a href="{{ route('game-start') }}" id="start-game">ՍԿՍԵԼ ԽԱՂԸ</a></li>
                            <li class="list-group-item"><a href="{{ route('leaders-page') }}">ԱՌԱՋԱՏԱՐՆԵՐԸ</a></li>
                            <li class="list-group-item"><a href="{{ route('regulations-page') }}">ԿԱՆՈՆՆԵՐ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>
@endsection

@section('script')
    <script>
        // Очищает URL
        if (window.location.hash && window.location.hash === '#_=_') {
            if (window.history && history.pushState) {
                window.history.pushState("", document.title, window.location.pathname);
            } else {
                // Prevent scrolling by storing the page current scroll offset
                let scroll = {
                    top: document.body.scrollTop,
                    left: document.body.scrollLeft
                };
                window.location.hash = '';
                // Restore the scroll offset, should be flicker free
                document.body.scrollTop = scroll.top;
                document.body.scrollLeft = scroll.left;
            }
        }
        // Очищает URL
    </script>
@endsection
