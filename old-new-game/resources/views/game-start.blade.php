@extends('layouts.front')

@section('content')
    <div class="container">
        <div class="row row-bg">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12 col-md-5 col-lg-5">
                        <img src="{{ asset('images/image-title.png') }}" alt="Title image">
                    </div>
                    <div class="col-sm-12 col-md-7 col-lg-7 timer-col">
                        <ul class="list-inline timer-score" id="timer-score">
                            <li class="list-inline-item"><img src="{{ asset('images/timer-clock.png') }}" alt="Timer">
                            </li>
                            <li class="list-inline-item"><span class="seconds">00</span></li>
                            <li class="list-inline-item"><span>ՄԻԱՎՈՐ</span></li>
                            <li class="list-inline-item"><span class="score">00</span></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p class="question-p">
                            <span class="question-number">Հարց 1</span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p class="question question-p">
                            {{ $question->question }}
                        </p>
                    </div>
                </div>
                <div class="row" id="dynamic-row">
                    @foreach($answers as $answer)
                        <div class="col-sm-12 col-md-4">
                            <div class="answer-block">

                                <form action="#" method="post" hidden>
                                    {{ csrf_field() }}
                                    <input type="hidden" name="questionnaire_id" value="{{ $questionnaire->id }}">
                                    <input type="hidden" name="first_question_id" value="{{ $question->id }}">
                                    <input type="hidden" name="question_score" value="{{ $question->score }}">
                                    <input type="hidden" name="truth" value="{{ ($answer->truth == 1) ? '1' : '0' }}">
                                </form>

                                <div class="answers-img-block mx-auto">
                                    <img src="{{ asset('images') . '/' . $answer->answer_photo }}"
                                         alt="{{ $answer->answer_photo }}">
                                </div>
                                <button type="button" class="btn btn-warning btn-block">{{ $answer->answer }}</button>
                            </div>
                        </div>
                    @endforeach
                    <form action="{{ route('save-result') }}" hidden method="post" id="save_result_form">
                        {{ csrf_field() }}
                        <input type="hidden" name="result" value="">
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>
@endsection

@section('script')
    <script>
        // Timer
        function getTimeRemaining(endtime) {
            let t = Date.parse(endtime) - Date.parse(new Date());
            let seconds = Math.floor((t / 1000) % 60);
            return {
                'total': t,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime) {
            let clock = document.getElementById(id);
            let secondsSpan = clock.querySelector('.seconds');

            function updateClock() {
                let t = getTimeRemaining(endtime);

                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if (t.total <= 0) {
                    clearInterval(timeinterval);
                    let result = $('span.score').text();
                    $('input[name="result"]').attr('value', result);
                    $('#save_result_form').submit();
                }
            }

            updateClock();
            let timeinterval = setInterval(updateClock, 1000);
        }

        let deadline = new Date(Date.parse(new Date()) + 60 * 1000);
        initializeClock('timer-score', deadline);
        // Timer
    </script>
@endsection
