<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'FrontController@index', 'as' => 'home']);

Route::group(['middleware' => ['auth', 'gameTime']], function () {
    Route::get('/start-page', ['uses' => 'FrontController@start_page', 'as' => 'start-page']);
    Route::get('/game-start', ['uses'=>'FrontController@start', 'as'=>'game-start']);
    Route::post('/ajax-call', ['uses' => 'FrontController@ajax_call', 'as' => 'ajax-call']);
    Route::post('/save-result', ['uses' => 'FrontController@save_result', 'as' => 'save-result']);
    Route::get('/score-page', ['uses' => 'FrontController@show_score', 'as' => 'score-page']);
    Route::get('/leaders-page', ['uses' => 'FrontController@show_leaders', 'as' => 'leaders-page']);
    Route::get('/regulations-page', ['uses' => 'FrontController@regulations', 'as' => 'regulations-page']);
});

// Admin
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role', 'gameTime']], function () {
    // Dashboard
    Route::get('/home', 'Admin\HomeController@index')->name('admin.dashboard');
    // Questions
    // Restore in trash
    Route::get('/question/restore/{id}', 'Admin\QuestionController@restore')->name('question.restore');
    // Final delete
    Route::post('/question/final-remove/{id}', 'Admin\QuestionController@final_remove')->name('question.final_remove');
    // Show all trashed question
    Route::get('/question-in-trash', 'Admin\QuestionController@get_trashed_questions')->name('question.in-trash');
    // Send trash question and answers
    Route::any('/check-answers', 'Admin\QuestionController@check_isset_answers')->name('question.check-answers');
    Route::resource('/question', 'Admin\QuestionController');
    // Collect questionnaire
    Route::resource('/questionnaire', 'Admin\QuestionnaireController');
    // User list
    Route::resource('/user', 'Admin\UserController');
});
// Admin
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
