<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use App\Question;
use App\Questionnaire;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(view()->exists('admin.questionnaire-list')) {
            $questionnaires = Questionnaire::all()->toArray();

            foreach ($questionnaires as $key => $questionnaire) {
                $questions_obj = [];
                $new_questionnaires[$key] = [
                    'id' => $questionnaire['id'],
                    'title' => $questionnaire['title'],
                    'created_at' => $questionnaire['created_at'],
                ];
                $questions_id = json_decode($questionnaire['questions_id']);
                if (is_array($questions_id)) {
                    foreach ($questions_id as $id) {
                        $question = Question::find($id);
                        array_push($questions_obj, $question);
                    }
                }
                $new_questionnaires[$key]['questions'] = $questions_obj;
            }
//            dd($new_questionnaires);
            return view('admin.questionnaire-list', compact('new_questionnaires'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questions = Question::all();
        if(view()->exists('admin.questionnaire-add')) {
            return view('admin.questionnaire-add', compact('questions'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'questions_id' => "present|array",
            'title' => 'required|max:255'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $inputs = $request->except('_token');

        $ids = explode(',', $request->questions_id[0]);
        $ids_json = json_encode($ids);

        $inputs['questions_id'] = $ids_json;

        $new_questionnaire = new Questionnaire();
        $new_questionnaire->fill($inputs);
        if ($new_questionnaire->save()) {
            return redirect('admin/questionnaire')->with('status', 'Successfully added new questionnaire');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Questionnaire::findOrFail($id);
        if ($question->delete()) {
            return redirect('admin/questionnaire')->with('status', 'Successfully added to trash');
        }
    }
}
