<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Question;
use App\Answer;
use Validator;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (view()->exists('admin.question-list')) {
            $questions = Question::orderBy('id', 'DESC')->paginate(5);
            return view('admin.question-list', compact('questions'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (view()->exists('admin.question-add')) {
            return view('admin.question-add');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required|max:255',
            'score' => 'required',
            'truth' => 'required',
            'answer1' => 'required|max:255',
            'images1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'answer2' => 'required|max:255',
            'images2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'answer3' => 'required|max:255',
            'images3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $question_info['question'] = $request->question;
        $question_info['score'] = $request->score;

        $new_question = new Question();
        $new_question->fill($question_info);

        // Save question
        if ($new_question->save()) {
            // Copy images
            foreach ($request->file() as $image) {
                $image->move(public_path() . '/images/', $image->getClientOriginalName());
            }
            // Answers info array
            $answers_info = [
                'answer1' => [
                    'answer' => $request->answer1,
                    'question_id' => $new_question->id,
                    'answer_photo' => $request->file('images1')->getClientOriginalName(),
                    'truth' => ('answer1' == $request->truth) ? 1 : 0
                ],
                'answer2' => [
                    'answer' => $request->answer2,
                    'question_id' => $new_question->id,
                    'answer_photo' => $request->file('images2')->getClientOriginalName(),
                    'truth' => ('answer2' == $request->truth) ? 1 : 0
                ],
                'answer3' => [
                    'answer' => $request->answer3,
                    'question_id' => $new_question->id,
                    'answer_photo' => $request->file('images3')->getClientOriginalName(),
                    'truth' => ('answer3' == $request->truth) ? 1 : 0
                ]
            ];
            // Copy answers
            foreach ($answers_info as $answer) {
                $new_answers = new Answer();
                $new_answers->fill($answer);
                $new_answers->save();
            }
            return redirect('admin/question')->with('status', 'Successfully added a new question');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::findOrFail($id);
        $answers = $question->answers;
        if (view()->exists('admin.question-edit')) {
            return view('admin.question-edit', [
                'question' => $question,
                'answers' => $answers
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->except(['_token', '_method']);
        $validator = Validator::make($request->all(), [
            'question' => 'required|max:255',
            'score' => 'required',
            'truth' => 'required',
            'answer1' => 'required|max:255',
            'answer2' => 'required|max:255',
            'answer3' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->route('question.edit', $id)->withErrors($validator);
        }

        if (!$request->file()) {
            // Question info
            $question_info = [
                'question' => $inputs['question'],
                'score' => $inputs['score'],
            ];

            // Answers info
            $answers_info = [
                $inputs['answer1_id'] => [
                    'answer' => $request->answer1,
                    'question_id' => $id,
                    'answer_photo' => $inputs['old_images1'],
                    'truth' => ('answer1' == $inputs['truth']) ? 1 : 0
                ],
                $inputs['answer2_id'] => [
                    'answer' => $request->answer2,
                    'question_id' => $id,
                    'answer_photo' => $inputs['old_images2'],
                    'truth' => ('answer2' == $inputs['truth']) ? 1 : 0
                ],
                $inputs['answer3_id'] => [
                    'answer' => $request->answer3,
                    'question_id' => $id,
                    'answer_photo' => $inputs['old_images3'],
                    'truth' => ('answer3' == $inputs['truth']) ? 1 : 0
                ]
            ];

            // Update
            $question = Question::find($id);
            $question->fill($question_info);
            if ($question->update()) {
                foreach ($answers_info as $key => $answer_arr) {
                    $answer = Answer::find($key);
                    $answer->fill($answer_arr);
                    $answer->update();
                }
            }
        } else {
            // Copy images
            $img_arr = [];
            foreach ($request->file() as $key => $image) {
                $image->move(public_path() . '/images/', $image->getClientOriginalName());
                $img_arr[$key] = $image->getClientOriginalName();
            }

            // Question info
            $question_info = [
                'question' => $inputs['question'],
                'score' => $inputs['score'],
            ];

            // Answers info
            $answers_info = [
                $inputs['answer1_id'] => [
                    'answer' => $request->answer1,
                    'question_id' => $id,
                    'answer_photo' => (array_key_exists('images1', $img_arr)) ? $img_arr['images1'] : $inputs['old_images1'],
                    'truth' => ('answer1' == $inputs['truth']) ? 1 : 0
                ],
                $inputs['answer2_id'] => [
                    'answer' => $request->answer2,
                    'question_id' => $id,
                    'answer_photo' => (array_key_exists('images2', $img_arr)) ? $img_arr['images2'] : $inputs['old_images2'],
                    'truth' => ('answer2' == $inputs['truth']) ? 1 : 0
                ],
                $inputs['answer3_id'] => [
                    'answer' => $request->answer3,
                    'question_id' => $id,
                    'answer_photo' => (array_key_exists('images3', $img_arr)) ? $img_arr['images3'] : $inputs['old_images3'],
                    'truth' => ('answer3' == $inputs['truth']) ? 1 : 0
                ]
            ];

            // Update
            $question = Question::find($id);
            $question->fill($question_info);
            if ($question->update()) {
                foreach ($answers_info as $key => $answer_arr) {
                    $answer = Answer::find($key);
                    $answer->fill($answer_arr);
                    $answer->update();
                }
            }
        }
        return redirect()->route('question.index')->with('status', 'Question and answers successfully update !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        if ($question->delete()) {
            foreach ($question->answers as $answer) {
                $answer->delete();
            }
            return 'Send to trash';
        }
    }





    public function get_trashed_questions()
    {
        if (view()->exists('admin.question-list-trashed')) {
            $questions = Question::onlyTrashed()->paginate(5);
            return view('admin.question-list-trashed', compact('questions'));
        }
    }

    public function restore($id)
    {
        $answers = Answer::withTrashed()->where('question_id', $id)->get();
        foreach ($answers as $answer) {
            $answer->restore();
        }
        Question::withTrashed()->findOrFail($id)->restore();
        return redirect('admin/question')->with('status', 'Question successfully restored from trash');
    }

    public function check_isset_answers(Request $request)
    {
        $question_id = $request->question_id;
        $question = Question::find($question_id);
        return $question->answers;
    }

    public function final_remove($id)
    {
        $answers = Answer::withTrashed()->where('question_id', $id)->get();
        foreach ($answers as $answer) {
            $answer->forceDelete();
        }
        if (Question::withTrashed()->findOrFail($id)->forceDelete()) {
            return redirect()->back()->with('status', 'The question was finally deleted');
        }
    }
}
