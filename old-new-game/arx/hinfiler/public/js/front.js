$(document).ready(function () {
    // Start game
    $(document).on('click', 'div.answer-block', function () {
        // Если правильный ответ то добавляем промежуточный результат
        var truth = $(this).find('input[name="truth"]').val();
        var questionScore = $(this).find('input[name="question_score"]').val();
        if (truth == 1) {
            let current_score_block = $('.score');
            let current_score = +current_score_block.text();
            current_score_block.text(current_score + +questionScore);
        }

        let currentForm = $(this).find('form');
        let formInfo = new FormData(currentForm[0]);

        $.ajax({
            type: 'POST',
            url: location.origin + '/ajax-call',
            data: formInfo,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                // alert(123);
            },
            success: function (data) {
                if (data) {
                    console.log(data);
                    $('p.question.question-p').text(data.question);
                    $('span.question-number').html('Հարց &nbsp; ' + data.question_number);
                    var blocks = [];
                    $('div.answer-block').each(function (key, value) {
                        blocks.push(value);
                    });
                    for (var i = 0; i < blocks.length; i++) {
                        blocks[i].querySelector('input[name="_token"]').value = $('meta[name="csrf-token"]').attr('content');
                        blocks[i].querySelector('input[name="questionnaire_id"]').value = data.questionnaire_id;
                        blocks[i].querySelector('input[name="first_question_id"]').value = data.question_id;
                        blocks[i].querySelector('input[name="question_score"]').value = data.score;
                        blocks[i].querySelector('input[name="truth"]').value = (data.answers[i].truth) ? 1 : 0;
                        blocks[i].querySelector('.answers-img-block.mx-auto > img').src = '/images/'+data.answers[i].answer_photo;
                        blocks[i].querySelector('button.btn.btn-warning.btn-block').innerText = data.answers[i].answer;
                    }
                } else {
                    console.log(data);
                }
            }
        });
    });
    // Start game
});


// TIMER
function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    return {
        'total': t,
        'seconds': seconds
    };
}
function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        var t = getTimeRemaining(endtime);

        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
            var result = $('span.score').text();
            // alert(result);
            $('input[name="result"]').attr('value', result);
            $('#save_result_form').submit();
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}
var deadline = new Date(Date.parse(new Date()) + 60 * 1000);
initializeClock('timer-score', deadline);
// TIMER
