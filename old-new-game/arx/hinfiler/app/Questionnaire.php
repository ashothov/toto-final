<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $fillable = ['title', 'questions_id'];

    protected $dates = ['deleted_at'];
}
