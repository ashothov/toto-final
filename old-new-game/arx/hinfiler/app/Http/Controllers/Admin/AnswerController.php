<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all answers
        $answers = Answer::paginate(9);
        // Get related question
        foreach ($answers as $key => $answer) {
            $question_array[$key] = [
                'id' => $answer->question->id,
                'value' => $answer->question->question
            ];
        }

        // Get related question (unique)
        $questions = [];
        foreach ($question_array as $item) {
            if (!in_array($item['id'], $questions)) {
                $questions[$item['id']] = $item['value'];
            }
        }
        if (view()->exists('admin.answer-list')) {
            return view('admin.answer-list', [
                'answers' => $answers,
                'questions' => $questions,
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (view()->exists('admin.answer-add')) {
            $questions = Question::select('id', 'question')->get();
            // Только те вопросы для котрых нет ответов
            $unanswered_questions = [];
            foreach ($questions as $question) {
                if ($question->answers->isEmpty()) {
                    array_push($unanswered_questions, $question);
                }
            }
            return view('admin.answer-add', compact('unanswered_questions'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $answers = [
            'answer1' => [
                'answer' => $request->answer1,
                'question_id' => $request->question,
                'answer_photo' => $request->file('images1')->getClientOriginalName(),
                'truth' => ('answer1' == $request->truth) ? 1 : 0
            ],
            'answer2' => [
                'answer' => $request->answer2,
                'question_id' => $request->question,
                'answer_photo' => $request->file('images2')->getClientOriginalName(),
                'truth' => ('answer2' == $request->truth) ? 1 : 0
            ],
            'answer3' => [
                'answer' => $request->answer3,
                'question_id' => $request->question,
                'answer_photo' => $request->file('images3')->getClientOriginalName(),
                'truth' => ('answer3' == $request->truth) ? 1 : 0
            ]
        ];

        $validate = Validator::make($request->all(), [
            'question' => 'required',
            'truth' => 'required',
            'answer1' => 'required|max:255',
            'images1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'answer2' => 'required|max:255',
            'images2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'answer3' => 'required|max:255',
            'images3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validate->fails()) {
            return redirect('admin/answer/create')->withErrors($validate)->withInput();
        }

        foreach ($request->file() as $image) {
            $image->move(public_path() . '/images/', $image->getClientOriginalName());
        }

        foreach ($answers as $answer) {
            $new_answers = new Answer();
            $new_answers->fill($answer);
            $new_answers->save();
        }
        return redirect('admin/answer')->with('status', 'Responses successfully received');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
