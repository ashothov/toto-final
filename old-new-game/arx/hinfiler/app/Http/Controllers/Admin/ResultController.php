<?php

namespace App\Http\Controllers\Admin;

use App\Result;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResultController extends Controller
{
    public function save_result(Request $request)
    {
        $inputs = $request->except('_token');

        $new_user_result = new Result();
        $new_user_result->fill($inputs);
        if ($new_user_result->save()) {
            $game_result = Result::orderBy('id', 'DESC')->limit(1)->get();
            if (view()->exists('front.score-page')) {
                return view('front.score-page', compact('game_result'));
            }
        }
    }
}
