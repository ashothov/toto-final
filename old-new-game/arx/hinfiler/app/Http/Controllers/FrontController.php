<?php

namespace App\Http\Controllers;

use App\Question;
use App\Questionnaire;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (view()->exists('front.index')) {
            return view('front.index');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function start_game(Request $request)
    {
        if (view()->exists('front.game-page')) {
            if($request->isMethod('get')) {
                $questionnaires_id = Questionnaire::where('id' ,'>' ,0)->pluck('id')->toArray();

                $random_id = rand(min($questionnaires_id), max($questionnaires_id));

                // Получаем случайный билет
                $questionnaire = Questionnaire::find($random_id);
                $questionnaire_array['questionnaire_id'] = $questionnaire->id;
                // dump($questionnaire);

                // Получаем идентификаторы вопросов из предыдущего билета
                $quest_id = json_decode($questionnaire->questions_id);
                // dump($quest_id);

                // Получаем первый вопрос со своими ответами
                $question = Question::find($quest_id[0]);
                // Вопрос
                //  dump($question->question);
                // Ответы
                // dd($question->answers);

                return view('front.game-page', [
                    'questionnaire' => $questionnaire,
                    'question' => $question
                ]);
            }
        }
    }

    public function ajax_call(Request $request)
    {
        // Выбираем билет, который уже сдается
        $questionnaire = Questionnaire::find($request->questionnaire_id);
        // Выбираем ID билета, который уже сдается
        $questionnaire_array['questionnaire_id'] = $questionnaire->id;
        // Получаем идентификаторы вопросов из билета, который уже сдается
        $quest_id = json_decode($questionnaire->questions_id);
        // Удаляем идентификатор первого вопроса из массива на который уже ответили

        $key = array_search($request->first_question_id, $quest_id);
        $key++;
        // Выбираем следующий вопрос
        if(array_key_exists($key, $quest_id)) {
            $question = Question::find($quest_id[$key]);
            $questionnaire_array['question_id'] = $question->id;
            $questionnaire_array['question'] = $question->question;
            $questionnaire_array['score'] = $question->score;
            $questionnaire_array['question_number'] = ++$key;

            // Ответы следующего вопроса
            $questionnaire_array['answers'] = $question->answers;

            return $questionnaire_array;
        } else {
            return null;
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_leaders()
    {

        if (view()->exists('front.leaders-page')) {
            return view('front.leaders-page');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_regulations()
    {
        if (view()->exists('front.regulations-page')) {
            return view('front.regulations-page');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_score()
    {
        if (view()->exists('front.score-page')) {
            return view('front.score-page');
        }
    }
}
