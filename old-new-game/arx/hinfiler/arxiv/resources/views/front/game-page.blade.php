@extends('layouts.front')

@section('title', 'Game page')

@section('content')
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <img src="{{ asset('images/image-title.png') }}" alt="Title image">
            </div>
            <div class="col-sm-6">
                <ul class="list-inline timer-score" id="timer-score">
                    <li class="list-inline-item"><img src="{{ asset('images/timer-clock.png') }}" alt="Timer"></li>
                    <li class="list-inline-item"><span class="seconds">00</span></li>
                    <li class="list-inline-item"><span>ՄԻԱՎՈՐ</span></li>
                    <li class="list-inline-item"><span class="score">00</span></li>
                </ul>
            </div>
        </div>
        {{-- DB info --}}
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="question-p">
                    <span class="question-number">Հարց &nbsp; 1</span>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p class="question question-p">
                    ({{ $question->id }})
                    {{ $question->question }}
                    ({{ $question->score }})
                </p>
            </div>
        </div>
        <div class="row">
            @foreach($question->answers as $answers)
            <div class="col-sm-12 col-md-4">
                <div class="answer-block"
                     data-questionnaire-id="{{ $questionnaire->id }}"
                     data-first-question-id="{{ $question->id }}"
                     data-question-score="{{ $question->score }}"
                     data-truth="{{ ($answers->truth == 1) ? '1' : '0' }}">
                    <div class="answers-img-block mx-auto">
                        <img src="{{ asset('images') . '/' . $answers->answer_photo }}" alt="{{ $answers->answer_photo }}">
                    </div>
                    <button type="button" class="btn btn-warning btn-block">{{ $answers->answer }}</button>
                </div>
            </div>
            @endforeach
        </div>
        {{-- DB info --}}
    </div>
@endsection
