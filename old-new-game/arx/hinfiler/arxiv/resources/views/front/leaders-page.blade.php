@extends('layouts.front')

@section('title', 'Leaders page')

@section('content')
    <div class="col-sm-12 col-md-6 offset-md-3 text-center">
        <img src="{{ asset('images/image-title.png') }}" alt="Title image">
        <div class="row">
            <div class="col-sm-12 col-md-10 offset-md-1 text-center">
                <div class="liders-block">
                    <h2>ԱՌԱՋԱՏԱՐՆԵՐԸ</h2>
                    <ol class="list-group liders-list">
                        <li class="list-group-item"><a href="#">1.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">135</span></a></li>
                        <li class="list-group-item"><a href="#">2.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">130</span></a></li>
                        <li class="list-group-item"><a href="#">3.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">125</span></a></li>
                        <li class="list-group-item"><a href="#">4.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">120</span></a></li>
                        <li class="list-group-item"><a href="#">5.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">105</span></a></li>
                        <li class="list-group-item"><a href="#">6.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">95</span></a></li>
                        <li class="list-group-item"><a href="#">7.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">90</span></a></li>
                        <li class="list-group-item"><a href="#">8.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">85</span></a></li>
                        <li class="list-group-item"><a href="#">9.&nbsp;&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp; <span class="user-score">70</span></a></li>
                        <li class="list-group-item"><a href="#">10.&nbsp; Armen Grigoryan &nbsp;&nbsp;&nbsp;  <span class="user-score">65</span></a></li>
                    </ol>
                </div>
                <a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>
            </div>
        </div>
    </div>
@endsection
