@extends('layouts.app')

@section('title', 'Answer list')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard.index') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Answers</li>
    </ol>
    <div class="card mb-3">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-12 col-md-10">
                    <label>
                        <select class="form-control custom-select">
                            <option selected>Select question</option>
                            @if(isset($questions) && is_array($questions))
                                @foreach($questions as $key => $question)
                                    <option value="{{ $key }}">{{ $question }}</option>
                                @endforeach
                            @endif
                        </select>
                    </label>
                </div>
                <div class="col-sm-12 col-md-2">
                    <a href="{{ route('answer.create') }}" role="button" class="btn btn-success float-right btn-sm">
                        Add new answer &nbsp;
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Images</th>
                        <th>Truth</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($answers) && is_object($answers))
                        @foreach($answers as $key => $answer)
                            @if (($key % 3) === 0)
                                <tr>
                                    <td>{{ $answer->id }}</td>
                                    <td>{{ $answer->question->question }}</td>
                                    <td>{{ $answer->answer }}</td>
                                    <td>
                                        <img src="{{ asset('images') . '/' . $answer->answer_photo }}" alt="Answer{{ $answer->id }}" width="50">
                                    </td>
                                    <td>
                                        <button type="button" class="btn {{ $answer->truth ? 'btn-success' : 'btn-danger' }}">
                                            <i class="fa {{ $answer->truth ? 'fa-check' : 'fa-times' }} fa-fw"></i>
                                        </button>
                                    </td>
                                    <td>
                                        {{-- update --}}
                                        <a href="{{ route('answer.edit', ['id' => $answer->id]) }}" role="button" class="btn btn-outline-info btn-sm">
                                            Edit <i class="fa fa-edit"></i>
                                        </a>
                                        {{-- update --}}
                                    </td>
                                    <td>
                                        {{-- Send to trash --}}
                                        <form action="{{ route('answer.destroy', $answer->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-outline-danger btn-sm">Send to trash <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                        {{-- Send to trash --}}
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td>{{ $answer->id }}</td>
                                    <td>{{ $answer->question->question }}</td>
                                    <td>{{ $answer->answer }}</td>
                                    <td>
                                        <img src="{{ asset('images') . '/' . $answer->answer_photo }}" alt="Answer{{ $answer->id }}" width="50">
                                    </td>
                                    <td>
                                        <button type="button" class="btn {{ $answer->truth ? 'btn-success' : 'btn-danger' }}">
                                            <i class="fa {{ $answer->truth ? 'fa-check' : 'fa-times' }} fa-fw"></i>
                                        </button>
                                    </td>
                                    <td>
                                        {{-- update --}}
                                        <a href="{{ route('answer.edit', ['id' => $answer->id]) }}" role="button" class="btn btn-outline-info btn-sm">
                                            Edit <i class="fa fa-edit"></i>
                                        </a>
                                        {{-- update --}}
                                    </td>
                                    <td>
                                        {{-- Send to trash --}}
                                        <form action="{{ route('answer.destroy', $answer->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-outline-danger btn-sm">Send to trash <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                        {{-- Send to trash --}}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="float-right">
                @if(isset($answers) && is_object($answers))
                    {{ $answers->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection
