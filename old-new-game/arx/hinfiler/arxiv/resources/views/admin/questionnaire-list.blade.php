@extends('layouts.app')

@section('title', 'Questionnaire list')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard.index') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Questionnaire</li>
    </ol>
    <div class="card mb-3">
        <div class="card-header">
            <label>Show
                <select class="form-control-sm custom-select" style="width: 70px;">
                    <option selected>10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                entries</label>
            <a href="{{ route('questionnaire.create') }}" role="button" class="btn btn-success float-right btn-sm">
                Collect a new questionnaire
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="card-body">

            <div class="accordion" id="accordion">
                @if(isset($new_questionnaires) && is_array($new_questionnaires))
                    @foreach($new_questionnaires as $key => $questionnaire)
                        <div class="card">
                            <div class="card-header">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $questionnaire['id'] }}" aria-expanded="true" aria-controls="collapseOne">
                                        <h5>{{ $questionnaire['id'] }} &nbsp; {{ $questionnaire['title'] }} &nbsp; {{ \Carbon\Carbon::parse($questionnaire['created_at'])->format('d / m / Y') }}</h5>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse{{ $questionnaire['id'] }}" class="collapse {{ $key ? '' : 'show' }}" data-parent="#accordion">
                                @foreach($questionnaire['questions'] as $question)
                                    <div class="table-responsive">
                                        <table class="table" style="margin-bottom: 0;">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">{{ $question->question }}</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Answer</th>
                                                <th scope="col">Answer photo</th>
                                                <th scope="col">Truth</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($question->answers as $answer)
                                                <tr>
                                                    <th>{{ $answer->id }}</th>
                                                    <td><b>{{ $answer->answer }}</b></td>
                                                    <td><img src="{{ asset('images') . '/' . $answer->answer_photo }}" width="50" height="50" alt="$answer->answer_photo"></td>
                                                    <td>
                                                        <button class="btn {{ ($answer->truth) ? 'btn-success' : 'btn-danger' }} btn-sm">
                                                            <i class="fa {{ ($answer->truth) ? 'fa-check' : 'fa-times' }} fa-fw"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
