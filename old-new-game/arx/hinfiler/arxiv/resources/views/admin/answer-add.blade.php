@extends('layouts.app')

@section('title', 'Add answer')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard.index') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('answer.index') }}">Answers</a>
        </li>
        <li class="breadcrumb-item active">Add answer</li>
    </ol>
    <div class="row">
        <div class="col-sm-12">
            <form action="{{ route('answer.store') }}" method="post" enctype="multipart/form-data" id="add_answers">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="question">Question</label>
                    <select name="question" class="form-control custom-select" id="question">
                        <option selected>Select question</option>
                        @if(isset($unanswered_questions) && is_array($unanswered_questions))
                            @foreach($unanswered_questions as $question)
                                <option value="{{ $question->id }}">{{ $question->question }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-9">
                        <div class="form-group">
                            <label for="answer1">Answer 1</label>
                            <input type="text" name="answer1" value="{{ old('answer1') }}" class="form-control" id="answer1" placeholder="Answer 1 ...">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Poster</label>
                            <br>
                            <label for="answer1_img" class="btn btn-success">
                                Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                            </label>
                            <input type="file" name="images1" class="form-control-file" hidden id="answer1_img">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-1">
                        <div class="form-group">
                            <label>True</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" value="answer1" id="customRadio1" name="truth" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio1"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-9">
                        <div class="form-group">
                            <label for="answer2">Answer 2</label>
                            <input type="text" name="answer2" value="{{ old('answer2') }}" class="form-control" id="answer2" placeholder="Answer 2 ...">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Poster</label>
                            <br>
                            <label for="answer2_img" class="btn btn-success">
                                Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                            </label>
                            <input type="file" name="images2" class="form-control-file" hidden id="answer2_img">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-1">
                        <div class="form-group">
                            <label>True</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" value="answer2" id="customRadio2" name="truth" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio2"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-9">
                        <div class="form-group">
                            <label for="answer3">Answer 3</label>
                            <input type="text" name="answer3" value="{{ old('answer3') }}" class="form-control" id="answer3" placeholder="Answer 3 ...">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Poster</label>
                            <br>
                            <label for="answer3_img" class="btn btn-success">
                                Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                            </label>
                            <input type="file" name="images3" class="form-control-file" hidden id="answer3_img">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-1">
                        <div class="form-group">
                            <label>True</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" value="answer3" id="customRadio3" name="truth" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio3"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success btn-block" id="add-answer">
                    Add answer
                </button>
            </form>
        </div>
    </div>
@endsection
