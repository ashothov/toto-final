@extends('layouts.app')

@section('title', 'Question list')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard.index') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Questions</li>
    </ol>
    <div class="card mb-3">
        <div class="card-header">
            <label>Show
                <select class="form-control-sm custom-select" style="width: 70px;">
                    <option selected>10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                entries</label>
            <a href="{{ route('question.create') }}" role="button" class="btn btn-success float-right btn-sm">
                Add new question &nbsp;
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Question</th>
                        <th>Score</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Edit</th>
                        <th>Add to trash</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($questions) && is_object($questions))
                        @foreach($questions as $question)
                    <tr>
                        <td>{{ $question->id }}</td>
                        <td>{{ $question->question }}</td>
                        <td>{{ $question->score }}</td>
                        <td>{{ \Carbon\Carbon::parse($question->created_at)->format('d.m.Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($question->created_at)->format('d.m.Y') }}</td>
                        <td>
                            {{-- update --}}
                            <a href="{{ route('question.edit', ['id' => $question->id]) }}" role="button" class="btn btn-outline-info btn-sm">
                                Edit <i class="fa fa-edit"></i>
                            </a>
                            {{-- update --}}
                        </td>
                        <td>
                            {{-- Send to trash --}}
                            <form action="{{ route('question.destroy', $question->id) }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-outline-danger btn-sm">Send to trash <i class="fa fa-trash"></i>
                                </button>
                            </form>
                            {{-- Send to trash --}}
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="float-right">
                @if(isset($questions) && is_object($questions))
                    {{ $questions->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection
