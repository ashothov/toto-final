$(document).ready(function () {

    // uploaded file count show in button
    $(':file').change(function () {
        let uploadedFiles = $(this)[0].files;
        let uploadedFilesLength = uploadedFiles.length;
        $(this).prev().find('.img-count').text(uploadedFilesLength);
    });
    // uploaded file count show in button



    /*$('#add_answers').on('submit', function (e) {
        e.preventDefault();
        let formInfo = new FormData($(this)[0]);

        $.ajax({
            url: location.origin + '/admin/answer',
            method: 'POST',
            data: formInfo,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                let myIcon = '<i class="fa fa-spinner fa-spin"></i>';
                $('#add-answer').append(myIcon);
            },
            success: function (data) {
                $('#add-answer').find('i').remove();
                console.log(data);
            }
        });
    });*/




    // Collect tickets
    /*$("#sortable1, #sortable2").sortable({
        connectWith: ".connectedSortable",
        // beforeStop: function (event, ui) {
        //     console.log(ui.item);
        // },
        // stop: function (event, ui) {
        //     console.log(event, ui);
        // },
        // update: function (event, ui) {
        //     console.log(ui.item);
        // }
    }).disableSelection();
    $('#question-form').submit(function () {
        let ids = [];
        $('#sortable1 div').each(function (key, value) {
            ids.push(value.id);
        });
        $('#question-id-form').attr('value', ids);
    });*/

    // Collect ticket



    // Start game
    $('.answer-block').click(function () {
        let data = {
            'questionnaire_id': $(this).data('questionnaire-id'),
            'first_question_id': $(this).data('first-question-id'),
            'question_score': $(this).data('question-score'),
            'truth': $(this).data('truth')
        };
        // Если правильный ответ то добавляем промежуточный ответ
        if (data.truth) {
            let current_score_block = $('.score');
            let current_score = +current_score_block.text();
            current_score_block.text(current_score + data.question_score);
        }

        // window.open('http://toto-gaming.com/start-game');
        $.ajax({
            type: 'POST',
            url: location.origin + '/start-game',
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                alert(123);
            },
            success: function (data) {
                console.log(data);

            }
        });
    });
    // Start game

});


// TIMER
function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    return {
        'total': t,
        'seconds': seconds
    };
}
function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        var t = getTimeRemaining(endtime);

        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
            // alert('Game over');
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}
var deadline = new Date(Date.parse(new Date()) + 60 * 1000);
initializeClock('timer-score', deadline);
// TIMER
