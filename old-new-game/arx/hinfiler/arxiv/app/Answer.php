<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes;

    protected $fillable = ['answer', 'question_id', 'answer_photo', 'truth'];

    protected $dates = ['deleted_at'];

    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
