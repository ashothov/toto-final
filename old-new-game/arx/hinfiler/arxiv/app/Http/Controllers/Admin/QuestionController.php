<?php

namespace App\Http\Controllers\Admin;

use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (view()->exists('admin.question-list')) {
            $questions = Question::orderBy('id', 'DESC')->paginate(10);
            return view('admin.question-list', compact('questions'));
        }
    }

    /**
     * Get all questions from trash
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_trashed_questions()
    {
        if (view()->exists('admin.question-list-trashed')) {
            $questions = Question::onlyTrashed()->paginate(5);
            return view('admin.question-list-trashed', compact('questions'));
        }
    }

    /**
     * // Restore in trash
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        Question::withTrashed()->findOrFail($id)->restore();
        return redirect('admin/question')->with('status', 'Question successfully restored from trash');
    }

    /**
     * Finale remove
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function final_remove($id)
    {
        if ($id) {
            Question::withTrashed()->findOrFail($id)->forceDelete();
            return redirect()->back()->with('status', 'The question was finally deleted');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (view()->exists('admin.question-add')) {
            return view('admin.question-add');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required|max:255',
            'score' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $inputs = $request->except('_token');
        $new_question = new Question();
        $new_question->fill($inputs);
        if ($new_question->save()) {
            return redirect('admin/question')->with('status', 'Successfully added a new question');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::findOrFail($id);
        if (view()->exists('admin.question-edit')) {
            return view('admin.question-edit', compact('question'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required|max:255',
            'score' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('question.edit', ['id' => $id])->withErrors($validator);
        }

        $inputs = $request->except('_token', '_method');
        $update_question = Question::find($id);
        $update_question->fill($inputs);
        if ($update_question->update()) {
            return redirect('admin/question')->with('status', 'Question successfully updated');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        if ($question->delete()) {
            return redirect('admin/question')->with('status', 'Question successfully send to trash');
        }
    }
}
