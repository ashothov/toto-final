<?php

namespace App\Http\Controllers;

use App\Question;
use App\Questionnaire;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (view()->exists('front.index')) {
            return view('front.index');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function start_game(Request $request)
    {
        if (view()->exists('front.game-page')) {
            if($request->isMethod('get')) {
                $questionnaires_id = Questionnaire::where('id' ,'>' ,0)->pluck('id')->toArray();

                $random_id = rand(min($questionnaires_id), max($questionnaires_id));

                // Получаем случайный билет
                $questionnaire = Questionnaire::find($random_id);
                $questionnaire_array['questionnaire_id'] = $questionnaire->id;
                // dump($questionnaire);

                // Получаем идентификаторы вопросов из предыдущего билета
                $quest_id = json_decode($questionnaire->questions_id);
                // dump($quest_id);

                // Получаем первый вопрос со своими ответами
                $question = Question::find($quest_id[0]);
                // Вопрос
//            dump($question->question);
                // Ответы
                // dd($question->answers);

                /*            $questionnaire = Questionnaire::find(1);
                            dump($questionnaire); // questionnaire where id=1

                            $quest_id = json_decode($questionnaire->questions_id);
                            dump($quest_id); // questions id

                            $question_answers_info = [];
                            foreach ($quest_id as $key => $id) {
                                $questions = Question::find($id);
                                array_push($question_answers_info, $questions->question);
                                array_push($question_answers_info, $questions->answers);
                                break;
                            }
                            dump($question_answers_info); // questions and answers*/

                return view('front.game-page', [
                    'questionnaire' => $questionnaire,
                    'question' => $question
                ]);
            }
        } else if($request->isMethod('post')) {
            dump($request);
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_leaders()
    {
        if (view()->exists('front.leaders-page')) {
            return view('front.leaders-page');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_regulations()
    {
        if (view()->exists('front.regulations-page')) {
            return view('front.regulations-page');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_score()
    {
        if (view()->exists('front.score-page')) {
            return view('front.score-page');
        }
    }
}
