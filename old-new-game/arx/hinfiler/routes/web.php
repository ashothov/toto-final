<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'web'], function () {
    Route::get('/', ['uses' => 'FrontController@index', 'as' => 'home']);
    Route::get('/start-game', ['uses' => 'FrontController@start_game', 'as' => 'start-game']);
    Route::get('/leaders-page', ['uses' => 'FrontController@show_leaders', 'as' => 'leaders-page']);
    Route::get('/regulations-page', ['uses' => 'FrontController@show_regulations', 'as' => 'regulations-page']);
    Route::get('/score-page', ['uses' => 'FrontController@show_score', 'as' => 'score-page']);
    Route::post('/ajax-call', ['uses' => 'FrontController@ajax_call', 'as' => 'ajax-call']);
    Route::post('/save-result', ['uses' => 'Admin\ResultController@save_result', 'as' => 'save-result']);
    Route::auth();
});



Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    // Admin home controller
    Route::resource('/dashboard', 'Admin\HomeController');

    // Questions controller
    // Get trash
    Route::get('/question/all-trash', 'Admin\QuestionController@get_trashed_questions')->name('question.all_trash');
    // Restore in trash
    Route::get('/question/restore/{id}', 'Admin\QuestionController@restore')->name('question.restore');
    // Final trash
    Route::post('/question/final-remove/{id}', 'Admin\QuestionController@final_remove')->name('question.final_remove');
    Route::resource('/question', 'Admin\QuestionController');
    // Questions controller

    // Answer controller
    Route::resource('/answer', 'Admin\AnswerController');
    // Answer controller

    // Collect questionnaire
    Route::resource('/questionnaire', 'Admin\QuestionnaireController');
    // Collect questionnaire
});



Auth::routes();
