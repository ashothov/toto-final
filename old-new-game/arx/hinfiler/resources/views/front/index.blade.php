@extends('layouts.front')

@section('title', 'Home page')

@section('content')
    <img src="{{ asset('images/ball1.png') }}" alt="Ball">
    <img src="{{ asset('images/ball2.png') }}" alt="Ball">
    <img src="{{ asset('images/ball3.png') }}" alt="Ball">
    <img src="{{ asset('images/tennis.png') }}" alt="Tennis">
    <img src="{{ asset('images/ball4.png') }}" alt="Ball">
    <div class="col-sm-12 col-md-6 offset-md-3 text-center">
        <img src="{{ asset('images/image-title.png') }}" alt="Title image">
        <h1>ՍՊՈՐՏ ՏՈՒՐՆԻՐ</h1>

        <div class="row">
            <div class="col-sm-12 col-md-10 offset-md-1 text-center">
                <ul class="list-group home-list">
                    <li class="list-group-item"><a href="{{ route('start-game') }}" id="start-game">ՍԿՍԵԼ ԽԱՂԸ</a></li>
                    <li class="list-group-item"><a href="{{ route('leaders-page') }}">ԱՌԱՋԱՏԱՐՆԵՐԸ</a></li>
                    <li class="list-group-item"><a href="{{ route('regulations-page') }}">ԿԱՆՈՆՆԵՐ</a></li>
                </ul>
                <a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>
            </div>
        </div>

    </div>
@endsection
