@extends('layouts.front')

@section('title', 'Score page')

@section('content')
    <div class="col-sm-12 col-md-6 offset-md-3 text-center">
        <img src="images/image-title.png" alt="Title image">
        <p class="score-p">{{ $game_result[0]->result }}</p>
        <p class="out-score">Ծեր միավորը</p>
        <a href="{{ route('leaders-page') }}" class="liders-a">Առաջատարնր</a>
        <a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>
    </div>
@endsection
