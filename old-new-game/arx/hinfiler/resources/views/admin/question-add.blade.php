@extends('layouts.app')

@section('title', 'Add question')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard.index') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('question.index') }}">Questions</a>
        </li>
        <li class="breadcrumb-item active">Add question</li>
    </ol>
    <div class="col-sm-12 col-md-8 offset-2">
        <form action="{{ route('question.store') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="question">Question</label>
                <input type="text" name="question" value="{{ old('question') }}" class="form-control" id="question" placeholder="Question text">
            </div>
            <div class="form-group">
                <label for="score">Score</label>
                <input type="text" name="score" value="{{ old('score') }}" class="form-control" id="score" placeholder="Score">
            </div>
            <br>
            <button type="submit" class="btn btn-success btn-block">Add question</button>
        </form>
    </div>
@endsection
