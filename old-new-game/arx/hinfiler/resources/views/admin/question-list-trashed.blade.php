@extends('layouts.app')

@section('title', 'Question list in trash')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('dashboard.index') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Question list in trash</li>
    </ol>
    <div class="card mb-3">
        <div class="card-header">
            <label>Show
                <select class="form-control-sm custom-select" style="width: 70px;">
                    <option selected>10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                entries</label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Question</th>
                        <th>Score</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Restore</th>
                        <th>Finally delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($questions) && is_object($questions))
                        @foreach($questions as $question)
                    <tr>
                        <td>{{ $question->id }}</td>
                        <td>{{ $question->question }}</td>
                        <td>{{ $question->score }}</td>
                        <td>{{ \Carbon\Carbon::parse($question->created_at)->format('d.m.Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($question->created_at)->format('d.m.Y') }}</td>
                        <td>
                            {{-- restore --}}
                            <a href="{{ route('question.restore', $question->id) }}" role="button" class="btn btn-outline-info btn-sm">
                                Restore <i class="fa fa-share-square"></i>
                            </a>
                            {{-- restore --}}
                        </td>
                        <td>
                            {{-- final remove --}}
                            <form action="{{ route('question.final_remove', $question->id) }}" onsubmit="return confirm('Are you sure ?');" method="post">
                                {{ csrf_field() }}
                                <button class="btn btn-outline-danger btn-sm">Finally delete <i class="fa fa-window-close"></i>
                                </button>
                            </form>
                            {{-- final remove --}}
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="float-right">
                @if(isset($questions) && is_object($questions))
                    {{ $questions->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection
