<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Result extends Model
{
    protected $fillable = ['user_id', 'result'];

    //    public function user()
//    {
//        return $this->belongsTo('App\User');
//    }
}
