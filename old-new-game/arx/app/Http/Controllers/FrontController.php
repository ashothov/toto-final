<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Question;
use App\Result;
use App\User;
use App\Questionnaire;

class FrontController extends Controller
{
    public function index()
    {
        if (view()->exists('front.index')) {
            return view('front.index');
        }
    }


    public function start_page()
    {
        if (view()->exists('start-page')) {
            return view('start-page');
        }
    }


    public function show_leaders()
    {
        $results = Result::orderBy('result', 'DESC')->get();
        foreach ($results as $key => $result) {
            $user = User::select('name')->where('id', $result->user_id)->get();
            $user_info[$key]['name'] = $user[0]->name;
            $user_info[$key]['result'] = $result->result;
            $user_info[$key]['id'] = $result->user_id;
        }
        if (view()->exists('leaders-page')) {
            return view('leaders-page', compact('user_info'));
        }
    }


    public function regulations()
    {
        if (view()->exists('regulations-page')) {
            return view('regulations-page');
        }
    }


    public function start()
    {
        // Генерируем случайные целые числа
        $questionnaires_id = Questionnaire::where('id' ,'>' ,0)->pluck('id')->toArray();
        $random_id = rand(min($questionnaires_id), max($questionnaires_id));

        // Получаем случайный билет
        $questionnaire = Questionnaire::find($random_id);
        $questions_id = json_decode($questionnaire->questions_id);

        // Получаем первый вопрос
        $question = Question::find($questions_id[0]);

        // Получаем ответы на первый вопрос
        $answers = $question->answers;

        if (view()->exists('game-start')) {
            return view('game-start', [
                'question' => $question,
                'answers' => $answers,
                'questionnaire' => $questionnaire,
            ]);
        }
    }


    public function ajax_call(Request $request)
    {
        // Выбираем билет, который уже сдается
        $questionnaire = Questionnaire::find($request->questionnaire_id);
        // Выбираем ID билета, который уже сдается
        $questionnaire_array['questionnaire_id'] = $questionnaire->id;
        // Получаем идентификаторы вопросов из билета, который уже сдается
        $quest_id = json_decode($questionnaire->questions_id);
        // Удаляем идентификатор первого вопроса из массива на который уже ответили

        $key = array_search($request->first_question_id, $quest_id);
        $key++;
        // Выбираем следующий вопрос
        if(array_key_exists($key, $quest_id)) {
            $question = Question::find($quest_id[$key]);
            $questionnaire_array['question_id'] = $question->id;
            $questionnaire_array['question'] = $question->question;
            $questionnaire_array['score'] = $question->score;
            $questionnaire_array['question_number'] = ++$key;

            // Ответы следующего вопроса
            $questionnaire_array['answers'] = $question->answers;

            return $questionnaire_array;
        } else {
            return null;
        }
    }


    public function save_result(Request $request)
    {
        $inputs = $request->except('_token');

        $new_user_result = new Result();
        $new_user_result->fill($inputs);
        if ($new_user_result->save()) {
            $game_result = Result::orderBy('id', 'DESC')->limit(1)->get();
            $user_id = $game_result[0]->user_id;
            $user_info = User::find($user_id);
            if (view()->exists('score-page')) {
                return view('score-page', [
                    'game_result' => $game_result,
                    'user_info' => $user_info,
                ]);
            }
        }
    }
}
