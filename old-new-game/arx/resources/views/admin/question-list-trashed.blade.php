@extends('layouts.app')

@section('title', 'Question list')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Questions</li>
    </ol>
    <div class="card mb-3">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Question</th>
                        <th>Score</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Edit</th>
                        <th>Add to trash</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($questions) && is_object($questions))
                        @foreach($questions as $question)
                            <tr>
                                <td>{{ $question->id }}</td>
                                <td>{{ $question->question }}</td>
                                <td>{{ $question->score }}</td>
                                <td>{{ \Carbon\Carbon::parse($question->created_at)->format('d.m.Y') }}</td>
                                <td>{{ \Carbon\Carbon::parse($question->created_at)->format('d.m.Y') }}</td>
                                <td>
                                    {{-- restore --}}
                                    <a href="{{ route('question.restore', $question->id) }}" role="button" class="btn btn-outline-info btn-sm">
                                        Restore <i class="fa fa-share-square"></i>
                                    </a>
                                    {{-- restore --}}
                                </td>
                                <td>
                                    {{-- final remove --}}
                                    <form action="{{ route('question.final_remove', $question->id) }}" onsubmit="return confirm('Are you sure ?');" method="post">
                                        {{ csrf_field() }}
                                        <button class="btn btn-outline-danger btn-sm">Finally delete <i class="fa fa-window-close"></i>
                                        </button>
                                    </form>
                                    {{-- final remove --}}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="float-right">
                @if(isset($questions) && is_object($questions))
                    {{ $questions->links() }}
                @endif
            </div>
        </div>
    </div>
    {{-- Answers modal --}}
    {{--<div class="modal fade bd-example-modal-lg" id="answers-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Answers related to this question <span class="text-danger">(they will also be sent to trash)</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-hover" id="related_answers" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Question</th>
                            <th>Images</th>
                            <th>Truth</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="data-trash-button" form="">I agree</button>
                </div>
            </div>
        </div>
    </div>--}}
    {{-- Answers modal --}}
@endsection

