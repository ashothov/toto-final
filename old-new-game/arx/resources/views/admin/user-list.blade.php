@extends('layouts.app')

@section('title', 'User list')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Users</li>
    </ol>
    <div class="card mb-3">
        {{--<div class="card-header">
            <label>Show
                <select class="form-control-sm custom-select" style="width: 70px;">
                    <option selected>10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                entries</label>
            <a href="{{ route('question.create') }}" role="button" class="btn btn-success float-right btn-sm">
                Add new question &nbsp;
                <i class="fa fa-plus"></i>
            </a>
        </div>--}}
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Avatar</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Result</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($user_arr) && is_array($user_arr))
                        @foreach($user_arr as $user)
                            <tr>
                                <td>{{ $user['id'] }}</td>
                                <td><img src="{{ $user['avatar'] }}" width="50" alt="{{ $user['avatar'] }}"></td>
                                <td>{{ $user['name'] }}</td>
                                <td>{{ $user['email'] }}</td>
                                <td>{{ $user['result'] }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            {{--<div class="float-right">
                @if(isset($questions) && is_object($questions))
                    {{ $questions->links() }}
                @endif
            </div>--}}
        </div>
    </div>
@endsection

