<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {{--<meta property="fb:app_id" content="173585263319693" />--}}
    {{--<meta property="og:url"           content="https://new-game.com/" />--}}
    {{--<meta property="og:type"          content="website" />--}}
    {{--<meta property="og:title"         content="My site" />--}}
    {{--<meta property="og:description"   content="The type Open Graph tag describes whether the object being shared is an article, a video, music, a book, a user profile or a website." />--}}
    {{--<meta property="og:site_name"     content="Vardan Gulqanyan"/>--}}
    {{--<meta property="og:image"            content="https://new-game.com/images/gagarina.jpg" />--}}
    {{--<meta property="og:image:width"      content="400" />--}}
    {{--<meta property="og:image:height"     content="300" />--}}

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css') }}">
    <title>@yield('title')</title>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '446470039134217',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v2.10'
            });
            FB.AppEvents.logPageView();
        };
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.0&appId=446470039134217&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</head>
<body>

<main class="vertical-center">
    @yield('content')
</main>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
@yield('script')
<script src="js/front.js"></script>
</body>
</html>

