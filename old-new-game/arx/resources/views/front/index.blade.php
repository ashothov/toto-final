@extends('layouts.front')

@section('title', 'Home page')

@section('content')
    <div class="container-fluid home-container-fluid">
        <img class="football-img" src="{{ asset('images/ball1.png') }}" alt="Ball">
        <img class="basketball-img" src="{{ asset('images/ball2.png') }}" alt="Ball">
        <img class="volley-ball" src="{{ asset('images/ball3.png') }}" alt="Ball">
        <img class="tennis" src="{{ asset('images/tennis.png') }}" alt="Tennis">
        <img class="tennis-ball" src="{{ asset('images/ball4.png') }}" alt="Ball">
        <div class="row logo-row">
            <div class="col-sm-12 text-center">
                <img src="{{ asset('images/image-title.png') }}" class="home-logo" alt="Title image">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="home-title">Սպորտ էքսպերտ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="home-p">Պատասխանի ՛ր սպորտային հարցերի և ստացի՛ր նվեր
                    Ինչքան շատ հարցերի պատասխանես,
                    այդքան մեծ է շահելու հավանականությունդ</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <a href="{{ url('/auth/facebook') }}" class="home-a"><i class="fa fa-facebook"></i> Սկսել խաղը</a>
                {{--<a class="home-a" href="#">Սկսել խաղը</a>--}}
            </div>
        </div>
        <p class="footer-p">Իսկ դու ինչքա՞ն մոտ ես սպորտին</p>
    </div>
@endsection

