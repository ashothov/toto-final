@extends('layouts.front')

@section('title', 'Leaders page')

@section('content')
    <div class="container">
        <div class="row row-bg leaders-row">
            <img src="{{ asset('images/ball1.png') }}" alt="Ball">
            <img src="{{ asset('images/ball2.png') }}" alt="Ball">
            <img src="{{ asset('images/ball3.png') }}" alt="Ball">
            <img src="{{ asset('images/tennis.png') }}" alt="Tennis">
            <img src="{{ asset('images/ball4.png') }}" alt="Ball">
            <div class="col-sm-12 col-md-10 offset-md-1 col-lg-6 offset-lg-3 text-center">
                <a href="{{route('start-page')}}">
                <img src="{{ asset('images/image-title.png') }}" alt="Title image">
                </a>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                        <div class="liders-block">
                            <h2>ԱՌԱՋԱՏԱՐՆԵՐԸ</h2>
                            @if(isset($user_info) && is_array($user_info))
                            <ol class="list-group liders-list">
                                @foreach($user_info as $key => $item)
                                <li class="list-group-item">
                                    <a href="#">{{ ++$key }}. {{ $item['name'] }} <span class="user-score">{{ $item['result'] }}</span></a>
                                </li>
                                @endforeach
                            </ol>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            {{--Back Button--}}
            <div class="col-sm-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center">
                <a href="{{route('start-page')}}">
                    <img style="width: 110px; margin-bottom:25px!important;" src="{{asset('images/back-button.png')}}">
                </a>
            </div>
            {{--Back Button End--}}

        </div>

    </div>
    <!--<a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>-->
@endsection
