@extends('layouts.app')

@section('title', 'Add question')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('question.index') }}">Questions</a>
        </li>
        <li class="breadcrumb-item active">Add question</li>
    </ol>
    <div class="col-sm-12 col-md-10 offset-1">
        <form action="{{ route('question.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{-- Question --}}
            <div class="form-group">
                <label for="question">Question</label>
                <input type="text" name="question" value="{{ old('question') }}" class="form-control" id="question" placeholder="question text">
            </div>
            {{-- Score --}}
            <div class="form-group">
                <label for="score">Score</label>
                <input type="text" name="score" value=1 class="form-control" id="score" placeholder="question score">
            </div>
            <br>
            <hr>
            <br>
            {{-- Answer --}}
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="answer1" value="{{ old('answer1') }}" class="form-control" placeholder="Answer 1... IMG size (250X250)px">
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label for="answer1_img" class="btn btn-success">
                            Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                        </label>
                        <input type="file" name="images1" class="form-control-file" hidden id="answer1_img">
                    </div>
                </div>
                <div class="col-sm-12 col-md-1">
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" value="answer1" id="customRadio1" name="truth" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio1" style="display: inherit;"></label>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            {{-- Aswer --}}
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="answer2" value="{{ old('answer2') }}" class="form-control" placeholder="Answer 2... IMG size (250X250)px">
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label for="answer2_img" class="btn btn-success">
                            Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                        </label>
                        <input type="file" name="images2" class="form-control-file" hidden id="answer2_img">
                    </div>
                </div>
                <div class="col-sm-12 col-md-1">
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" value="answer2" id="customRadio2" name="truth" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio2" style="display: inherit;"></label>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            {{-- Answer --}}
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="form-group">
                        <input type="text" name="answer3" value="{{ old('answer3') }}" class="form-control" placeholder="Answer 3... IMG size (250X250)px">
                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <div class="form-group">
                        <label for="answer3_img" class="btn btn-success">
                            Choose <span class="img-count">...</span> <i class="fa fa-upload"></i>
                        </label>
                        <input type="file" name="images3" class="form-control-file" hidden id="answer3_img">
                    </div>
                </div>
                <div class="col-sm-12 col-md-1">
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" value="answer3" id="customRadio3" name="truth" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio3" style="display: inherit;"></label>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            {{-- Button --}}
            <button type="submit" class="btn btn-success btn-block">
                Add question
            </button>
        </form>
    </div>
@endsection
