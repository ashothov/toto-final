@extends('layouts.app')

@section('content')
    <div class="admin-dashboard">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Dashboard</div>

                        <div class="card-body">
                            {{--@if (session('status'))--}}
                                {{--<div class="alert alert-success">--}}
                                    {{--{{ session('status') }}--}}
                                {{--</div>--}}
                            {{--@endif--}}
                            For Reseting the Score Results of last week, press on this RESET RESULTS button!

                        </div>
                        <div class="card-header">
                            <a href="{{ route('refresh-results') }}" role="button" style="font-size: 20px;" class="btn btn-danger float-left btn-sm">
                                RESET RESULTS &nbsp;
                                {{--<i class="fa fa-plus"></i>--}}
                                <i class="fa fa-refresh fa-spin fa-1x fa-fw"></i>
                                <span class="sr-only">Loading...</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
