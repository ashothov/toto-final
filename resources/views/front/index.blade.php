<!doctype html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:image" content="https://game.it-talents.org/images/fb-toto-q.png">
    <meta property="og:url" content="https://game.it-talents.org/" />


    <meta property="og:description" content="'Խաղի արդյունքում ես հավաքել եմ 7777 միավոր'" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css') }}">
    <title>ՍՊՈՐՏ ԷՔՍՊԵՐՏ</title>

</head>
<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '446470039134217',
            xfbml      : true,
            version    : 'v3.1'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<main class="vertical-center">

    <div class="container-fluid home-container-fluid">
        <img class="football-img" src="{{ asset('images/ball1.png') }}" alt="Ball">
        <img class="basketball-img" src="{{ asset('images/ball2.png') }}" alt="Ball">
        <img class="volley-ball" src="{{ asset('images/ball3.png') }}" alt="Ball">
        <img class="tennis" src="{{ asset('images/tennis.png') }}" alt="Tennis">
        <img class="tennis-ball" src="{{ asset('images/ball4.png') }}" alt="Ball">
        <div class="row logo-row">
            <div class="col-sm-12 text-center">
                <img src="{{ asset('images/image-title.png') }}" class="home-logo" alt="Title image">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="home-title">Սպորտ էքսպերտ</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="home-p">Պատասխանի՛ր սպորտային հարցերին և ստացի՛ր նվեր
                    Ինչքան շատ հարցերի պատասխանես,
                    այդքան մեծ է շահելուդ հավանականությունը</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <a href="{{ url('/auth/facebook') }}" id="game-start" class="home-a">
                    Մուտք գործեք Facebook-ի միջոցով
                </a>
            </div>
        </div>
        <p class="footer-p">Իսկ դու ինչքա՞ն մոտ ես սպորտին</p>
    </div>
</main>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<script src="{{asset('js/front.js')}}"></script>
{{--<script defer src="{{asset('js/sharegg.js')}}"></script>--}}

</body>
</html>

