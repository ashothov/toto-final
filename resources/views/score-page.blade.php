@extends('layouts.front')

@section('title', 'TotoGaming')
@section('styles')

    <style>
        .jssocials-share-logo:after{
            font-family: 'noto_sans_armenianXCnXBd';
            content: '  Share On Facebook';
        }
        .jssocials-share-link {
            font-family: 'noto_sans_armenianXCnXBd';
            background: #f14100;
            padding: 27px 30px 24px;
            border-radius: 15px;
            border-color: #f14100;
            font-size: 32px;
            width: 380px;
            display: inline-block;
            color: white!important;
            text-decoration: none;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid home-container-fluid">
        <img class="football-img" src="{{ asset('images/ball1.png') }}" alt="Ball">
        <img class="basketball-img" src="{{ asset('images/ball2.png') }}" alt="Ball">
        <img class="volley-ball" src="{{ asset('images/ball3.png') }}" alt="Ball">
        <img class="tennis" src="{{ asset('images/tennis.png') }}" alt="Tennis">
        <img class="tennis-ball" src="{{ asset('images/ball4.png') }}" alt="Ball">

        <div class="col-sm-12 col-md-8 offset-md-2 text-center">
            <a href="{{route('start-page')}}"><img src="{{ asset('images/image-title.png') }}"
                                                   alt="Title image" style="margin-top:25px!important;"></a><br>
        <!--<img src="{{ $user_info->avatar }}" alt="">-->
            <p class="out-score">{{ $user_info->name }}</p>
            <hr>
            <p class="out-score">Ձեր միավորները</p>
            @if(isset($game_result) && is_object($game_result))
                <p class="score-p">{{ $game_result[0]->result }}</p>
            @elseif(isset($game_result_low))
                <p class="score-p">{{ $game_result_low }}</p>
                <span style="color: white;">Այս շաբաթ Դուք ավելի շատ միավորներ եք հավաքել քան այս խաղի ժամանակ։</span>
            @endif
            <br>
            <br>
            <br>


            <div id="share">
                    <div class="row" style="display: block;">
                        <div class="col-xs-12" style="margin-bottom: 5px;">
                            <span class="score-page-a score-page-a-left" style="font-size: 20px; cursor: pointer;">Share On Facebook</span>
                        </div>
                    </div>

            </div>

            {{--OLD SHARE NATIVE--}}
            {{--<div onclick="return shareOverrideOGMeta();">--}}
                {{--<div class="row" style="display: block;">--}}
                    {{--<div class="col-xs-12" style="margin-bottom: 5px;">--}}
                            {{--<span class="score-page-a score-page-a-left" style="font-size: 20px; cursor: pointer;">Share Your Results In Facebook</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--OLD SHARE NATIVE--}}


            <div class="container-fluid score-page-a-fluid">
                <div class="row" style="display: block;">
                    <div class="col-xs-12" style="margin-bottom: 20px;">
                        <a href="{{ route('leaders-page') }}" class="score-page-a score-page-a-left">ԱՌԱՋԱՏԱՐՆԵՐ</a>
                    </div>
                </div>

                <div class="row" style="display: block;">
                    <div class="col-xs-12">
                        <a href="{{ route('regulations-page') }}" class="score-page-a score-page-a-right">ԿԱՆՈՆՆԵՐ</a>
                    </div>
                </div>
            </div>
        </div>

        {{--Back Button--}}
        <div class="col-sm-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center">
            <a href="{{route('start-page')}}">
                <img style="width: 110px; margin-bottom:25px!important;" src="{{asset('images/back-button.png')}}">
            </a>
        </div>
        {{--Back Button End--}}
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
    <script src="{{asset('js/jssocials/jssocials.min.js')}}"></script>

    <script>

        var result = document.querySelector('p.score-p').innerHTML;
        var link = "https://game.it-talents.org/";
        $("#share").jsSocials({
            showLabel: false,
            //label: 'Խաղի արդյունքում ես հավաքել եմ ' + result + ' միավոր',
            url: link,
//            text : 'Խաղի արդյունքում ես հավաքել եմ ' + 55 + ' միավոր',

            showCount: false,
            shareIn: "popup",
            shares: ["facebook"]
        });

    </script>
    <script>
//        function shareOverrideOGMeta() {
//            let result = document.querySelector('p.score-p').innerHTML;
//            var FBPic = "https://game.it-talents.org/images/fb-toto-share.png";
//            FB.ui({
//                method: 'share_open_graph',
//                action_type: 'og.likes',
//                action_properties: JSON.stringify({
//                    object: {
//                        'og:image': FBPic,
//                        'og:url': 'https://game.it-talents.org/save-result',
//                        'og:title': 'TotoGaming',
//                        'og:image:width': '1200',
//                        'og:image:height': '630',
//                        'og:description': 'Խաղի արդյունքում ես հավաքել եմ ' + result + ' միավոր',
//                        'og:image:type': 'image/jpeg'
//                    }
//                })
//            },
////                function (response) {
////                    window.location.replace("https://game.it-talents.org/start-page");
////                }
//            );
//        }
    </script>
@endsection
