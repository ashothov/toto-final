@extends('layouts.front')

@section('title', 'Regulations page')

@section('content')
    <div class="container">
        <div class="row row-bg">
            <img src="{{ asset('images/ball1.png') }}" alt="Ball">
            <img src="{{ asset('images/ball2.png') }}" alt="Ball">
            <img src="{{ asset('images/ball3.png') }}" alt="Ball">
            <img src="{{ asset('images/tennis.png') }}" alt="Tennis">
            <img src="{{ asset('images/ball4.png') }}" alt="Ball">
            <div class="col-sm-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center">
                <a href="{{route('start-page')}}">
                <img src="{{ asset('images/image-title.png') }}" alt="Title image">
                </a>
                <div class="row">
                    <div class="col-sm-12 col-md-10 offset-md-1 text-center">
                        <p class="regulations-p">
                            <span class="regulations-title">Կանոններ</span>
                            Փորձի՛ր սպորտային գիտելիքներդ, ստացի՛ր մրցանակ<br><br>
– Յուրաքանչյուր 24 ժամը մեկ դու ստանում ես խաղին մասնակցելու մեկ հնարավորություն<br><br>
– Քո  գրանցած ամենաբարձր արդյունքը գրանցվում է քո անվան դիմաց ԱՌԱՋԱՏԱՐՆԵՐ բաժնում: Խաղից հետո կարող ես տեսնել քո գրանցած արդյունքը, ինչպես նաև առաջատարների ցանկը<br><br>
– Ամեն շաբաթվա ավարտից հետո ամփոփվում են տվյալ շաբաթվա արդյունքները, վերջնական արդյունքում գրանցվում է Ձեր հավաքած ամենաբարձր միավորը:<br><br>
– Այն մասնակիցները, ովքեր տվյալ շաբաթվա ընթացքում տվել են ամենամեծ քանակությամբ ճիշտ պատասխաններ և հայտնվել են առաջատարների ցանկի առաջին 5 տեղերում, ստանում են մրցանակներ<br><br>
– Շաբաթվա արդյունքները ամփոփվում են 2 աշխատանքային օրվա ընթացքում<br><br>
– Մրցանակները շնորհվում են արդյունքները ամփոփելուց հետո մեկ շաբաթվա ընթացքում<br><br>
– Առաջին տեղի մասնակցի հաշվին կփոխանցվի 20000դ freebet , 2-րդ տեղի մասնակցի հաշվին՝ 10000դ freebet , իսկ 3–ից 5–րդ տեղի մասնակիցները կստանան ֆիլմի տոմս<br>
</p>
                    </div>
                </div>
            </div>
            {{--Back Button--}}
            <div class="col-sm-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center">
                <a href="{{route('start-page')}}">
                    <img style="width: 110px; margin-bottom:25px!important;" src="{{asset('images/back-button.png')}}">
                </a>
            </div>
            {{--Back Button End--}}
        </div>
    </div>
    <!--<a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>-->
@endsection
