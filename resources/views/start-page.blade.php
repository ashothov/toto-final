@extends('layouts.front')

@section('title', 'TotoGaming Sport Expert')

@section('content')
    <div class="container center-container">
        <div class="row row-bg">
            <img src="{{ asset('images/ball1.png') }}" alt="Ball">
            <img src="{{ asset('images/ball2.png') }}" alt="Ball">
            <img src="{{ asset('images/ball3.png') }}" alt="Ball">
            <img src="{{ asset('images/tennis.png') }}" alt="Tennis">
            <img src="{{ asset('images/ball4.png') }}" alt="Ball">
            <div class="col-sm-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-center">
                <img src="{{ asset('images/image-title.png') }}" alt="Title image">
                <h1 style="font-size: 46px;">ՍՊՈՐՏ ԷՔՍՊԵՐՏ</h1>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-10 offset-lg-1 text-center">
                        <ul class="list-group home-list">
                            <li class="list-group-item"><a href="{{ route('game-start') }}" {{ $result ? 'data-true' : 'data-false' }} id="start-game">ՍԿՍԵԼ ԽԱՂԸ</a></li>
                            <li class="list-group-item"><a href="{{ route('leaders-page') }}">ԱՌԱՋԱՏԱՐՆԵՐԸ</a></li>
                            <li class="list-group-item"><a href="{{ route('regulations-page') }}">ԿԱՆՈՆՆԵՐ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<a href="#" class="toto-link">www.<span class="toto-link-span">toto</span>gaming.am</a>-->

    <!-- Запрещено играть -->
    @if(session('status'))
        <div class="modal fade" id="not" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ծանուցում</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ session('status') }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger " data-dismiss="modal">Փակել</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- Запрещено играть -->
@endsection

@section('script')
    <script>
        // Очищает URL
        if (window.location.hash && window.location.hash === '#_=_') {
            window.location.replace("https://game.it-talents.org/start-page");
        }
        // Очищает URL

        // Открываем модальное окно которое оповещает о том что еще не прошло 24 часа
        $(document).ready(function () {
            var notModal = $('#not');
            if (notModal) {
                notModal.modal('show');
            }
        });
        // Открываем модальное окно которое оповещает о том что еще не прошло 24 часа

    </script>
@endsection
