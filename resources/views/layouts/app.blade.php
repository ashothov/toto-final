    <!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{ url('/') }}">TotoGaming</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        @auth
            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a class="nav-link" href="{{ route('reset-results') }}">
                        <i class="fa fa-fw fa-dashboard"></i>
                        <span class="nav-link-text">Results Page</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                    <a class="nav-link" href="{{ route('question.index') }}">
                        <i class="fa fa-fw fa-question-circle-o"></i>
                        <span class="nav-link-text">Questions</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                    <a class="nav-link" href="{{ route('questionnaire.index') }}">
                        <i class="fa fa-fw fa-file-code-o"></i>
                        <span class="nav-link-text">Collect ticket</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                    <a class="nav-link" href="{{ route('user.index') }}">
                        <i class="fa fa-fw fa-user-circle-o"></i>
                        <span class="nav-link-text">Users</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents"
                       data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-trash"></i>
                        <span class="nav-link-text">Trash</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="collapseComponents">
                        <li>
                            <a href="{{ route('question.in-trash') }}">Questions</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="navbar-nav sidenav-toggler">
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link text-center" id="sidenavToggler">--}}
                        {{--<i class="fa fa-fw fa-angle-left"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
            </ul>
        @endauth
        <ul class="navbar-nav ml-auto">
            @auth
                {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown"--}}
                       {{--aria-haspopup="true" aria-expanded="false">--}}
                        {{--<i class="fa fa-fw fa-envelope"></i>--}}
                        {{--<span class="d-lg-none">Messages<span--}}
                                {{--class="badge badge-pill badge-primary">12 New</span></span>--}}
                        {{--<span class="indicator text-primary d-none d-lg-block"><i class="fa fa-fw fa-circle"></i></span>--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">--}}
                        {{--<h6 class="dropdown-header">New Messages:</h6>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">--}}
                            {{--<strong>David Miller</strong>--}}
                            {{--<span class="small float-right text-muted">11:21 AM</span>--}}
                            {{--<div class="dropdown-message small">Hey there! This new version of SB Admin is pretty--}}
                                {{--awesome!--}}
                                {{--These messages clip off when they reach the end of the box so they don't overflow over--}}
                                {{--to--}}
                                {{--the sides!--}}
                            {{--</div>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">--}}
                            {{--<strong>Jane Smith</strong>--}}
                            {{--<span class="small float-right text-muted">11:21 AM</span>--}}
                            {{--<div class="dropdown-message small">I was wondering if you could meet for an appointment at--}}
                                {{--3:00--}}
                                {{--instead of 4:00. Thanks!--}}
                            {{--</div>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">--}}
                            {{--<strong>John Doe</strong>--}}
                            {{--<span class="small float-right text-muted">11:21 AM</span>--}}
                            {{--<div class="dropdown-message small">I've sent the final files over to you for review. When--}}
                                {{--you're able to sign off of them let me know and we can discuss distribution.--}}
                            {{--</div>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item small" href="#">View all messages</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown"--}}
                       {{--aria-haspopup="true" aria-expanded="false">--}}
                        {{--<i class="fa fa-fw fa-bell"></i>--}}
                        {{--<span class="d-lg-none">Alerts--}}
              {{--<span class="badge badge-pill badge-warning">6 New</span>--}}
            {{--</span>--}}
                        {{--<span class="indicator text-warning d-none d-lg-block">--}}
              {{--<i class="fa fa-fw fa-circle"></i>--}}
            {{--</span>--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">--}}
                        {{--<h6 class="dropdown-header">New Alerts:</h6>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">--}}
              {{--<span class="text-success">--}}
                {{--<strong>--}}
                  {{--<i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>--}}
              {{--</span>--}}
                            {{--<span class="small float-right text-muted">11:21 AM</span>--}}
                            {{--<div class="dropdown-message small">This is an automated server response message. All--}}
                                {{--systems--}}
                                {{--are online.--}}
                            {{--</div>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">--}}
              {{--<span class="text-danger">--}}
                {{--<strong>--}}
                  {{--<i class="fa fa-long-arrow-down fa-fw"></i>Status Update</strong>--}}
              {{--</span>--}}
                            {{--<span class="small float-right text-muted">11:21 AM</span>--}}
                            {{--<div class="dropdown-message small">This is an automated server response message. All--}}
                                {{--systems--}}
                                {{--are online.--}}
                            {{--</div>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item" href="#">--}}
              {{--<span class="text-success">--}}
                {{--<strong>--}}
                  {{--<i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>--}}
              {{--</span>--}}
                            {{--<span class="small float-right text-muted">11:21 AM</span>--}}
                            {{--<div class="dropdown-message small">This is an automated server response message. All--}}
                                {{--systems--}}
                                {{--are online.--}}
                            {{--</div>--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-divider"></div>--}}
                        {{--<a class="dropdown-item small" href="#">View all alerts</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
            @endauth
            {{--<li class="nav-item">
                <form class="form-inline my-2 my-lg-0 mr-lg-2">
                    <div class="input-group">
                        <input class="form-control" type="text" placeholder="Search for...">
                        <span class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
                    </div>
                </form>
            </li>--}}
            @guest
                {{--<li class="nav-item">
                    <a href="{{ route('login') }}" class="nav-link">
                        <i class="fa fa-fw fa-sign-in"></i> {{ __('Login') }}
                    </a>
                </li>--}}
                {{--<li class="nav-item">
                    <a href="{{ route('register') }}" class="nav-link">
                        <i class="fa fa-fw fa-pencil"></i> {{ __('Register') }}
                    </a>
                </li>--}}
            @else
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#logout_form_modal">
                        <i class="fa fa-fw fa-sign-out"></i>{{ __('Logout') }}
                    </a>
                </li>
            @endguest
        </ul>
    </div>
</nav>

<div class="content-wrapper" >
    <div class="container-fluid">
        {{-- Error message --}}
        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <ul>
                    @foreach($errors->all() as $error)
                        <li><b>{{ $error }}</b></li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        {{-- Error message --}}
        {{-- Success message --}}
        @if(session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <b>{{ session('status') }}</b>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        {{-- Success message --}}
        @yield('content')
    </div>
</div>

@auth
    <div class="content-wrapper">
        <!-- Footer -->
        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Copyright © Your Website 2018</small>
                </div>
            </div>
        </footer>
        <!-- /Footer -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- /Scroll to Top Button-->
    </div>

    <!-- Logout Modal-->
    <div class="modal fade" id="logout_form_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">@csrf</form>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="button"
                            onclick="document.getElementById('logout-form').submit();">Logout
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Logout Modal-->
@endauth


<div class="load-bg" id="load-bg" style="
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    background: #0000008c;
    color: #fff;
    font-size: 50px;
    text-align: center;
    line-height: 100vh;
    z-index: 10000;">
    <i class="fa fa-refresh fa-spin"></i>
</div>
<script src="{{ asset('js/jquery.min.js') }}" defer></script>
<script src="{{ asset('js/jquery-ui.min.js') }}" defer></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}" defer></script>
<script src="{{ asset('js/jquery.easing.min.js') }}" defer></script>
<script src="{{ asset('js/admin.js') }}" defer></script>
</body>
</html>

