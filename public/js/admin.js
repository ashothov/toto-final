$(document).ready(function () {

    // uploaded file count show in button
    $(':file').change(function () {
        let uploadedFiles = $(this)[0].files;
        let uploadedFilesLength = uploadedFiles.length;
        $(this).prev().find('.img-count').text(uploadedFilesLength);
    });
    // uploaded file count show in button

    // Show answers in modal window
    $('.question_trash_form').on('submit', function (e) {
        e.preventDefault();
        let formInfo = new FormData($(this)[0]);
        let trash_form_id = $(this).attr('id');
        $.ajax({
            url: location.origin + '/admin/check-answers',
            method: 'POST',
            data: formInfo,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('#load-bg').css('display', 'block');
            },
            success: function (data) {
                if (data) {
                    let answers_html = answers_data(data);
                    $('#related_answers tbody').html(answers_html);
                    $('#data-trash-button').attr('form', trash_form_id);
                    $('#load-bg').css('display', 'none');
                    $('#answers-modal').modal();
                }
            }
        });
    });
    // Show answers in modal window

    // Send trash question and answers
    $('#data-trash-button').on('click', function () {
        let form_id = $(this).attr('form');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: location.origin + '/admin/question/' + form_id,
            method: 'DELETE',
            data: {id: form_id, _method: 'DELETE'},
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('#load-bg').css('display', 'block');
            },
            success: function (data) {
                if (data) {
                    let message  = '<div class="alert alert-success" role="alert">'+ data +'</div>';
                    $('#answers-modal').modal('hide');
                    $('#load-bg').css('display', 'none');
                    $('.content-wrapper .container-fluid').prepend(message);
                    setInterval(function () {
                        location.reload();
                    }, 1000);
                }
            }
        });
    });
    // Send trash question and answers


    // Tickets sortable
    $("#sortable1, #sortable2").sortable({
        connectWith: ".connectedSortable",
    }).disableSelection();
    $('#question-form').submit(function () {
        let ids = [];
        $('#sortable1 div').each(function (key, value) {
            ids.push(value.id);
        });
        $('#question-id-form').attr('value', ids);
    });
    // Tickets sortable








    // Created HTML in modal
    function answers_data(obj) {
        let row = '';
        for(let key in obj) {
            row += '<tr>';
            row += '<td>'+ obj[key].id +'</td>';
            row += '<td>'+ obj[key].answer +'</td>';
            row += '<td><img src="'+ location.origin +'/images/'+ obj[key].answer_photo +'" width="60"></td>';
            if (obj[key].truth) {
                row += '<td style="text-align: center; vertical-align: middle; font-size: 30px; color: green;"><i class="fa fa-check-circle"></i></td>';
            } else {
                row += '<td style="text-align: center; vertical-align: middle; font-size: 30px; color: red;"><i class="fa fa-times-circle"></i></td>';
            }
            row += '</tr>';
        }
        return row;
    }
    // Created HTML in modal
});
