(function(window, $, jsSocials, undefined) {

    $.extend(jsSocials.shares, {

       

        facebook: {
            label: "Like",
            logo: "fa fa-facebook",
            shareUrl: "https://facebook.com/sharer/sharer.php?u=https://game.it-talents.org",
            countUrl: "https://graph.facebook.com/?id={url}",
            getCount: function(data) {
                return data.share && data.share.share_count || 0;
            }
        }

       

    });

}(window, jQuery, window.jsSocials));

